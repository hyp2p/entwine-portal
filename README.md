# HYP2P - entwine-portal

Server and dashboard that proxies requests from a 'portal' server over p2p/wrtc to an 'localhost' client allocated in the browser.
Like LocalTunnel but over wrtc.
Look at entwine-client for the client project for full client<->client http/ws proxy.
This is a playground project not intended for production env.

## Deploy

* `npm install`
* `npm start`

## Development

* `npm install`
* For backend: `npm run dev`
* For frontend: `npm run dev:ui`
