import { Agent } from "http";
import { Server } from "net";
import { Portal as _pm } from '../hyp2p/portal'

export class TunnelAgent extends Agent {
    private waitingCreateConn: any = [];
    public server: Server;
    private closed: any = false;

    constructor(subdomain: string, address: string) {
        super({
            keepAlive: false,
            maxFreeSockets: 1
        });
        this.server = _pm.init(subdomain, address);
        this.server.once("error", (err) => {
            throw `Agent server error: ${err}, [${subdomain}] socket is being closed`;
        });
        this.server.on("freeSocket", (socket) => {
            socket.resume();
            const cb = this.waitingCreateConn.shift();
            if (cb) {
                console.log("Agent: giving socket to queued conn request");
                setTimeout(() => { cb(null, socket); }, 0);
            } else {
                console.log("No missing callbacks :D");
            }
        });
    }

    _onClose() {
        this.closed = true;
        console.log("Agent: closed tcp socket");
        for (const conn of this.waitingCreateConn) {
            conn(new Error("closed"), null);
        }
        this.waitingCreateConn = [];
        this.server.emit("end");
    }

    createConnection(opt: any, cb: any) {
        if (this.closed) {
            cb(new Error("closed"), null);
            return;
        }
        this.waitingCreateConn.push(cb);
        setTimeout(() => {
            this.server.emit("awaitingSocket");
        }, 0);
    }

    destroy() {
        this.server.close();
        super.destroy();
    }
}
