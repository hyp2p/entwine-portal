import { Request, Response, Router, NextFunction } from 'express';
import { Client } from "./client";
import { Persistence as p } from '../database/mongodb';
import _ash from 'express-async-handler'
import tldjs from 'tldjs';
const myTldjs = tldjs.fromUserSettings({ validHosts: ['localhost'] });
import wss from '../bittorrent-tracker/wss-tracker';
import { Socket } from 'net';

export class ClientManager {
    private clients: Map<string, Client>;

    constructor() {
        this.clients = new Map();
        if (process.env.PERSISTENCE && process.env.PERSISTENCE == 'true') {
            p.connect().then(response => { console.log(`Persistence [${response}]`) });
        }
    }

    get middleware() {
        return _ash(async (req: Request, res: Response, next: NextFunction) => {
            const subdomain = req.headers.host ? myTldjs.getSubdomain(req.headers.host) : null;
            if (!req.query.hyp2p) {
                if (subdomain) {
                    const client: Client | null = await this.getClient(subdomain);
                    if (client) {
                        client.handleRequest(req, res);
                    } else {
                        res.render("empty", {
                            domain: req.headers.host?.replace(`${subdomain}.`, ''),
                            subdomain: subdomain
                        });
                    }
                } else {
                    //landing page?
                    next();
                }
            } else {
                switch (req.query.hyp2p) {
                    case 'entangle':
                        if (req.query.address && req.query.subdomain) {
                            res.json({
                                status: 'updated',
                                response: await this.upsertClient(<string>req.query.subdomain, <string>req.query.address)
                            });
                        } else {
                            res.json({ error: 'Missing [address|subdomain] query param' });
                        }
                        break;
                    default:
                        res.status(404);
                }

            }
        });
    }

    public handleWsUpdate = (req: Request, socket: Socket, head: any) => {
        const subdomain = req.headers.host ? myTldjs.getSubdomain(req.headers.host) : null;
        if (subdomain) {
            this.getClient(subdomain).then((client: Client | null) => {
                if (client) {
                    client.handleUpgrade(req, socket);
                } else {
                    socket.write('HTTP/1.1 403 Forbidden\r\n\r\n');
                    socket.destroy();
                }
            }).catch((err) => {
                socket.write('HTTP/1.1 500 Server Error\r\n\r\n');
                socket.destroy();
            });
        } else {
            if (req.url === '/api') {
                //do some logic to validate is only from customers
                wss.handleUpgrade(req, socket, head, (ws: any) => {
                    wss.emit('connection', ws, req);
                });
            }
        }
    };


    public async upsertClient(subdomain: string, address: string) {
        await p.upsertPeerServer(subdomain, address);
        this.stopClient(subdomain);
        return 'ok';
    }

    private async getClient(subdomain: any) {
        let c = this.clients.get(subdomain);
        if (c) {
            return c;
        } else {
            let ps: p.PeerServerModel | null = await p.getPeerServer(subdomain);
            if (ps) {
                const client = new Client(ps.subdomain, ps.address);
                this.clients.set(ps.subdomain, client);
                const _ps = ps;
                client.once("close", () => {
                    console.log("Manager: removing closed client: %s", _ps.subdomain);
                    const clientToClose = this.clients.get(_ps.subdomain);
                    if (!clientToClose) { return; }
                    this.clients.delete(_ps.subdomain);
                    clientToClose.close();
                });
                console.log("Manager: client initialized: %s", ps.subdomain);
                return client
            }
            return null;
        }
    }

    private stopClient(subdomain: any) {
        console.log("Manager: stoping client: %s", subdomain);
        const client = this.clients.get(subdomain);
        if (!client) {
            return;
        }
        this.clients.delete(subdomain);
        client.close();
    }
}
