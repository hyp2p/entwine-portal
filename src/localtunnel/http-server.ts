import express, { Request, Response } from 'express';
import path from "path";
import { ClientManager } from './client-manager';

const app = express();
const manager = new ClientManager();

app.set('views', path.join(__dirname, '../views'));
app.set('view engine', 'ejs');
app.use(manager.middleware);
app.use(express.json());
app.use(express.static(path.join(__dirname, '../hyp2p-ui/')));
app.get("/", (req: Request, res: Response) => {
    res.sendFile(path.join(__dirname + '../hyp2p-ui/index.html'));
});
app.get("*", (req: Request, res: Response) => {
    res.sendFile(path.join(__dirname + '../hyp2p-ui/index.html'));
});

export default { app: app, handleWsUpgrade: manager.handleWsUpdate.bind(manager) };