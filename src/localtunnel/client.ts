import { request, IncomingMessage } from "http";
import { EventEmitter } from "events";
import pump from 'pump';
import { TunnelAgent } from "./tunnel-agent";
import { Request, Response } from "express";
import BugoutSocket from "../bugout/bugout-socket";
import { Socket } from "net";
export class Client extends EventEmitter {
    public agent: TunnelAgent;

    constructor(subdomain: string, address: string) {
        super();
        this.agent = new TunnelAgent(subdomain, address);
    }

    close() {
        this.agent.destroy();
        this.emit("close");
    }

    handleRequest(req: Request, res: Response) {
        const opt = {
            path: req.url,
            agent: this.agent,
            method: req.method,
            headers: req.headers,
        };
        const clientReq = request(opt, (clientRes: IncomingMessage) => {
            res.writeHead(clientRes.statusCode || 200, clientRes.headers);
            pump(clientRes, res);
        });
        clientReq.once("error", (err) => {
            res.send(err);
        });
        pump(req, clientReq);
    }

    handleUpgrade(req: any, socket: Socket | BugoutSocket) {
        socket.once("error", (err: any) => {
            if (err.code == "ECONNRESET" || err.code == "ETIMEDOUT") {
                return;
            }
            console.error(err);
        });
        this.agent.createConnection({}, (err: any, conn: any) => {
            console.log("< [up] %s", req.url);
            if (err) {
                socket.end();
                return;
            }
            if (!socket.readable || !socket.writable) {
                conn.destroy();
                socket.end();
                return;
            }
            const arr = [`${req.method} ${req.url} HTTP/${req.httpVersion}`];
            for (let i = 0; i < req.rawHeaders.length - 1; i += 2) {
                arr.push(`${req.rawHeaders[i]}: ${req.rawHeaders[i + 1]}`);
            }
            arr.push("");
            arr.push("");
            pump(conn, socket);
            pump(socket, conn);
            conn.write(arr.join("\r\n"));
        });
    }
}
