import { Collection, FilterQuery, MongoClient } from "mongodb";

export namespace Persistence {
    let client: MongoClient | null;
    let peerCollection: Collection<PeerServerModel> | null;

    export async function connect() {
        if (process.env.CONNECTION_STRING) {
            try {
                const _client = new MongoClient(process.env.CONNECTION_STRING, { useUnifiedTopology: true });
                await _client.connect();
                const database = _client.db("hyp2pDb");
                peerCollection = database.collection("peerServers");
                client = _client;
                return "Connected";
            } catch (e) {
                console.error(e);
                return "Error during connection";
            }
        } else {
            return "Invalid CONNECTION_STRING"
        }
    }

    export async function disconnect() {
        await client?.close();
        return "disconnected";
    }

    export async function upsertPeerServer(subdomain: string, address: string) {
        return exec(async () => {
            return peerCollection?.updateOne({ subdomain }, {
                $set: { subdomain, address, updated: new Date() }
            }, { upsert: true });
        });
    }

    export function getPeerServer(subdomain: string): Promise<PeerServerModel | null> | null {
        let filter: FilterQuery<PeerServerModel> = { subdomain: subdomain };
        return peerCollection ? peerCollection?.findOne(filter).then((p) => {
            return p;
        }) : null;
    }

    async function exec(func: any) {
        if (client && client.isConnected()) {//#Connected
            return func();
        } else {
            return 'Cant access data, db is offline';
        }
    }

    export interface PeerServerModel {
        subdomain: string;
        address: string;
        updated: Date;
    }
}