import { EventEmitter } from "events";
import WebTorrent from 'webtorrent';
import * as bencode from "bencode";
import { box, randomBytes, sign, hash } from "tweetnacl";
import bs58 from "bs58";
import bs58check from 'bs58check';
import RIPEMD160 from 'ripemd160';
import debug from "debug";
interface Torrent extends WebTorrent.Torrent {
    wires: any[];
    discovery: any;
}

interface BufferWithName extends Buffer {
    name: any;
}

export default class Bugout extends EventEmitter {
    private debug: debug.Debugger = debug("bugout");
    private EXT = "bo_channel";
    private PEERTIMEOUT = 5 * 60 * 1000;
    private SEEDPREFIX = "490a";
    private torrent: Torrent;
    private announce: any;
    private wt: WebTorrent.Instance;
    public seed: any;
    private timeout: any;
    private keyPair: any;
    private keyPairEncrypt: any;
    private pk: any;
    private ek: any;
    private identifier: any;
    private peers: any = {};
    private seen: any = {};
    private lastwirecount: any = null;
    private api: any = {};
    private callbacks: any = {};
    private serveraddress: any = null;
    private heartbeattimer: any = null;

    constructor(identifier: string | object | null, opts?: any) {
        super();
        // TODO: option to pass shared secret to encrypt swarm traffic
        if (identifier && typeof (identifier) == "object") {
            opts = identifier;
            identifier = null;
        }
        opts = opts || {};
        const trackeropts = opts.tracker || {};
        //trackeropts.getAnnounceOpts = trackeropts.getAnnounceOpts || function() { return {numwant: 4}; };
        if (opts.iceServers)
            trackeropts.rtcConfig = { iceServers: opts.iceServers };
        this.announce = opts.announce || ["wss://hub.bugout.link", "wss://tracker.openwebtorrent.com", "wss://tracker.btorrent.xyz"];
        this.wt = opts.wt || new WebTorrent(Object.assign({ tracker: trackeropts }, opts["wtOpts"] || {}));
        if (opts["seed"]) {
            this.seed = opts["seed"];
        } else {
            this.seed = this.encodeseed(randomBytes(32));
        }

        this.timeout = opts["timeout"] || this.PEERTIMEOUT;
        this.keyPair = opts["keyPair"] || sign.keyPair.fromSeed(Uint8Array.from(bs58check.decode(this.seed)).slice(2));
        // ephemeral encryption key only used for this session
        this.keyPairEncrypt = box.keyPair();

        this.pk = bs58.encode(this.keyPair.publicKey);
        this.ek = bs58.encode(this.keyPairEncrypt.publicKey);

        this.identifier = identifier || this.getAddress();// <== this instantiate the address if there is NO identifier
        this.peers = {}; // list of peers seen recently: address -> pk, ek, timestamp
        this.seen = {}; // messages we've seen recently: hash -> timestamp
        this.lastwirecount = null;

        this.debug("address", this.getAddress(), "identifier", this.identifier, "public key", this.pk.toString().substring(20), "encryption key", this.ek);// <== this instantiate the address if there is AN identifier

        //Probably we can use `npm install web-file` if we want to transpile for browsers
        // if (typeof (File) == "object") {
        //     blob = new File([this.identifier], this.identifier);
        // } else {
        //     blob = Buffer.from(this.identifier);
        //     blob.name = this.identifier; // why is that for?
        // }
        const _opts = Object.assign({ "name": this.identifier, "announce": this.announce },
            opts["torrentOpts"] || {});
        const _bfId: BufferWithName = <BufferWithName>Buffer.from(this.identifier);
        _bfId.name = this.identifier;
        this.torrent = <Torrent>this.wt.seed(_bfId, _opts, (torrent: any) => {
            this.handleSeed(torrent);
        });
        this.torrent.on("wire", (wire: any, addr?: string) => {
            this.attach(this.identifier, wire, addr);
        });
        if (opts.heartbeat) {
            this.heartbeat(opts.heartbeat);
        }
    }

    public getAddress() {
        return this.encodeaddress(this.keyPair.publicKey)
    }

    private handleSeed(torrent: Torrent) {
        this.emit("torrent", this.identifier, torrent);
        //what means: TypeError: Cannot read property 'tracker' of null.... what is discovery and how to handle it?
        //this happened when turning on=>off and then off=>on while a request was in progress
        if (torrent.discovery.tracker) {
            torrent.discovery.tracker.on("update", (update: any) => {
                this.emit("tracker", this.identifier, update);
            });
        }
        torrent.discovery.on("trackerAnnounce", () => {
            this.emit("announce", this.identifier);
            this.connections();
        });
    }

    private encodeseed(material: Uint8Array) {
        return bs58check.encode(Buffer.concat([
            Buffer.from(this.SEEDPREFIX, "hex"),
            Buffer.from(material)
        ]));
    }

    private encodeaddress(material: Uint8Array, addrPrefix: string = "55") {
        return bs58check.encode(
            Buffer.concat([
                Buffer.from(addrPrefix, "hex"),
                new RIPEMD160().update(Buffer.from(hash(material))).digest()
            ])
        );
    }

    // start a heartbeat and expire old "seen" peers who don't send us a heartbeat
    private heartbeat(interval: number = 30000) {
        this.heartbeattimer = setInterval(() => {
            // broadcast a 'ping' message
            this.ping();
            const t = this.now();
            // remove any 'peers' entries with timestamps older than timeout
            for (const p in this.peers) {
                const pk = this.peers[p].pk;
                const address = this.resolveAddress(pk);
                const last = this.peers[p].last;
                if (last + this.timeout < t) {
                    delete this.peers[p];
                    this.emit("timeout", address);
                    this.emit("left", address);
                }
            }
        }, interval);
    }

    public destroy(cb: (err: Error | string) => void) {
        clearInterval(this.heartbeattimer);
        const packet = this.makePacket({ "y": "x" });
        this.sendRaw(packet);
        this.wt.remove(this.torrent, undefined, cb);
    }

    public close(cb: (err: Error | string) => void) {
        this.destroy(cb);
    }

    private connections() {
        if (this.torrent.wires.length != this.lastwirecount) {
            this.lastwirecount = this.torrent.wires.length;
            this.emit("connections", this.torrent.wires.length);
        }
        return this.lastwirecount;
    }

    public resolveAddress(pk: any) {
        if (typeof (pk) == "string") {
            return this.encodeaddress(bs58.decode(pk))
        }
        if (pk.length == 32) {
            return this.encodeaddress(pk)
        }
        console.log(pk);
        throw 'Cant resolve address, must be string or pk.length=32, obtained:' + pk;
    }

    public ping() {
        const packet = this.makePacket({ "y": "p" });
        this.sendRaw(packet);
    }

    public send(address: any, message: any) {
        if (!message) {
            message = address;
            address = null;
        }
        let packet = this.makePacket({ "y": "m", "v": JSON.stringify(message) });
        if (address) {
            if (this.peers[address]) {
                packet = this.encryptPacket(this.peers[address].pk, packet);
            } else {
                throw address + " not seen - no public key.";
            }
        }
        this.sendRaw(packet);
    }

    public register(call: string, fn: CallableFunction, docstring?: string) {
        this.api[call] = fn;
        this.api[call].docstring = docstring;
    }

    public rpc(address: string, call: string | object, args: string | object, callback?: Function) {
        // my kingdom for multimethods lol
        // calling styles:
        // address, call, args, callback
        // address, call, callback (no args)
        // call, args, callback (implicit server address)
        // call, callback (no args, implicit server address)
        if (this.serveraddress && typeof (args) == "function") {
            callback = args;
            args = call;
            call = address;
            address = this.serveraddress;
        }
        if (this.peers[address]) {
            let pk = this.peers[address].pk;
            let callnonce = randomBytes(8);
            this.callbacks[this.toHex(callnonce)] = callback;
            this.makeEncryptSendPacket(pk, { "y": "r", "c": call, "a": JSON.stringify(args), "rn": callnonce });
        } else {
            throw address + " not seen - no public key.";
        }
    }

    // outgoing

    private makePacket(params: { [key: string]: any }): Buffer {
        let p: any = {
            "t": this.now(),
            "i": this.identifier,
            "pk": this.pk,
            "ek": this.ek,
            "n": randomBytes(8),
        };
        for (const k in params) {
            p[k] = params[k];
        }
        const pe = bencode.encode(p);
        return bencode.encode({
            "s": sign.detached(pe, this.keyPair.secretKey),
            "p": pe,
        });
    }

    private encryptPacket(pk: any, packet: any): Buffer {
        if (this.peers[this.resolveAddress(pk)]) {
            const nonce = randomBytes(box.nonceLength);
            const e = box(packet, nonce, bs58.decode(this.peers[this.resolveAddress(pk)].ek), this.keyPairEncrypt.secretKey);
            return bencode.encode({
                "n": nonce,
                "ek": this.ek,
                "e": e,
            });
        } else {
            throw this.resolveAddress(pk) + " not seen - no encryption key.";
        }
    }

    public sendRaw(message: Buffer) {
        this.debug("sendRaw message:", message)
        let wires: any[] = this.torrent.wires;
        for (let w = 0; w < wires.length; w++) {
            const extendedhandshake = wires[w]["peerExtendedHandshake"];
            if (extendedhandshake && extendedhandshake.m && extendedhandshake.m[this.EXT]) {
                wires[w].extended(this.EXT, message);
            }
        }
        const _hash = this.toHex(hash(message).slice(16));
        this.debug("sent", _hash, "to", wires.length, "wires");
    }

    public makeEncryptSendPacket(pk: any, packet: { [key: string]: any }) {
        this.sendRaw(this.encryptPacket(pk, this.makePacket(packet)));
    }

    // incoming
    public rpcCall(pk: any, call: string, args: any, nonce: any, callback?: CallableFunction) {
        let packet: any = { "y": "rr", "rn": nonce };
        if (this.api[call]) {
            this.api[call](this.resolveAddress(pk), args,
                (result: any) => {
                    packet["rr"] = JSON.stringify(result);
                    this.makeEncryptSendPacket(pk, packet);
                });
        } else {
            packet["rr"] = JSON.stringify({ "error": "No such API call." });
            this.makeEncryptSendPacket(pk, packet);
        }
    }

    public sawPeer(pk: any, ek: any, identifier: string) {
        const address = this.resolveAddress(pk);
        const t = this.now();
        this.debug("sawPeer", address, t);
        if (address != this.getAddress()) {
            if (!this.peers[address] || this.peers[address].last + this.timeout < t) {
                this.peers[address] = { "ek": ek, "pk": pk, "last": t, };
                this.debug("seen", address);
                this.emit("seen", address);
                if (address == this.identifier) {
                    this.serveraddress = address;
                    this.debug("seen server", address);
                    this.emit("server", address);
                }
                this.sayHi();
            } else {
                this.peers[address].ek = ek;
                this.peers[address].last = t;
            }
        }
    }

    private sayHi() {
        this.sendRaw(this.makePacket({ "y": "p" }));
    }

    public attach(identifier: string, wire: any, addr: any) {
        const _name: string = this.EXT;
        const _bugout = this;
        const _bencode = bencode;
        class WireHandler {
            wire: any;
            constructor(w: any) {
                this.wire = w;
                this.wire.extendedHandshake.id = identifier;
                this.wire.extendedHandshake.pk = _bugout.pk;
                this.wire.extendedHandshake.ek = _bugout.ek;
            }
            get name(): string {
                return _name;
            }
            get onExtendedHandshake() {
                return (handshake: { [key: string]: any }) => {
                    _bugout.debug("wire extended handshake!",
                        _bugout.resolveAddress(handshake.pk.toString()),
                        this.wire.peerId, handshake);

                    _bugout.emit("wireseen", _bugout.torrent.wires.length, wire);
                    _bugout.connections();
                    // TODO: check sig and drop on failure - wire.peerExtendedHandshake
                    _bugout.sawPeer(handshake.pk.toString(), handshake.ek.toString(), identifier);
                }
            }
            get onMessage() {
                return (message: Buffer) => {
                    _bugout.debug("wire on message!");
                    const hash_ = _bugout.toHex(hash(message).slice(16));
                    const t = _bugout.now();
                    _bugout.debug("raw message ", identifier, message.length, hash);
                    if (!_bugout.seen[hash_]) {
                        let unpacked = _bencode.decode(message);
                        if (unpacked.e && unpacked.n && unpacked.ek) {
                            const ek = unpacked.ek.toString();
                            _bugout.debug("message encrypted by", ek, unpacked);
                            const decrypted = box.open(unpacked.e, unpacked.n, bs58.decode(ek), _bugout.keyPairEncrypt.secretKey);
                            if (decrypted) {
                                unpacked = _bencode.decode(<Buffer>decrypted);
                            } else {
                                unpacked = null;
                            }
                        }
                        // if there's no data decryption failed
                        if (unpacked && unpacked.p) {
                            _bugout.debug("unpacked message", unpacked);
                            const packet = _bencode.decode(unpacked.p);
                            const pk = packet.pk.toString();
                            const id = packet.i.toString();
                            const checksig = sign.detached.verify(unpacked.p, unpacked.s, bs58.decode(pk));
                            const checkid = id == identifier;
                            const checktime = packet.t + _bugout.timeout > t;
                            _bugout.debug("packet", packet);
                            if (checksig && checkid && checktime) {
                                // message is authenticated
                                _bugout.sawPeer(pk, packet.ek.toString(), identifier);
                                // check packet types
                                if (packet.y == "m") {
                                    _bugout.debug("message", identifier, packet);
                                    const messagestring = packet.v.toString();
                                    let messagejson = null;
                                    try {
                                        messagejson = JSON.parse(messagestring);
                                    } catch (e) {
                                        _bugout.debug("Malformed message JSON: " + messagestring);
                                    }
                                    if (messagejson) {
                                        _bugout.emit("message", _bugout.resolveAddress(pk), messagejson, packet);
                                    }
                                } else if (packet.y == "r") { // rpc call
                                    _bugout.debug("rpc", identifier, packet);
                                    const call = packet.c.toString();
                                    const argsstring = packet.a.toString();
                                    let args = null;
                                    try {
                                        args = JSON.parse(argsstring);
                                    } catch (e) {
                                        debug("Malformed args JSON: " + argsstring);
                                    }
                                    const _nonce = packet.rn;
                                    _bugout.emit("rpc", _bugout.resolveAddress(pk), call, args, _bugout.toHex(_nonce));
                                    // make the API call and send back response
                                    _bugout.rpcCall(pk, call, args, _nonce);
                                } else if (packet.y == "rr") { // rpc response
                                    const nonce = _bugout.toHex(packet.rn);
                                    if (_bugout.callbacks[nonce]) {
                                        let responsestring, responsestringstruct = null;
                                        if (typeof (packet["rr"]) != "undefined") {
                                            responsestring = packet.rr.toString();
                                        } else {
                                            debug("Empty rr in rpc response.");
                                        }
                                        try {
                                            responsestringstruct = JSON.parse(responsestring);
                                        } catch (e) {
                                            debug("Malformed response JSON: " + responsestring);
                                        }
                                        if (_bugout.callbacks[nonce] && responsestringstruct) {
                                            _bugout.debug("rpc-response", _bugout.resolveAddress(pk), nonce, responsestringstruct);
                                            _bugout.emit("rpc-response", _bugout.resolveAddress(pk), nonce, responsestringstruct);
                                            _bugout.callbacks[nonce](responsestringstruct);
                                            delete _bugout.callbacks[nonce];
                                        } else {
                                            _bugout.debug("RPC response nonce not known:", nonce);
                                        }
                                    } else {
                                        _bugout.debug("dropped response with no callback.", nonce);
                                    }
                                } else if (packet.y == "p") {
                                    const address = _bugout.resolveAddress(pk);
                                    _bugout.debug("ping from", address);
                                    _bugout.emit("ping", address);
                                } else if (packet.y == "x") {
                                    const address = _bugout.resolveAddress(pk);
                                    _bugout.debug("got left from", address);
                                    delete _bugout.peers[address];
                                    _bugout.emit("left", address);
                                } else {
                                    // TODO: handle ping/keep-alive message
                                    _bugout.debug("unknown packet type");
                                }
                            } else {
                                _bugout.debug("dropping bad packet", hash, checksig, checkid, checktime);
                            }
                        } else {
                            _bugout.debug("skipping packet with no payload", hash, unpacked);
                        }
                        // forward first-seen message to all connected wires
                        // TODO: block flooders
                        _bugout.sendRaw(message);
                    } else {
                        _bugout.debug("already seen", hash);
                    }
                    // refresh last-seen timestamp on this message
                    _bugout.seen[hash_] = _bugout.now();
                }
            }

        }
        wire.use(WireHandler);
        wire.on("close", () => {
            this.detach(wire);
        });
    }

    public detach(wire: any) {
        this.emit("wireleft", this.torrent.wires.length, wire);
        this.connections();
    }

    private now() {
        return (new Date()).getTime();
    }

    // https://stackoverflow.com/a/39225475/2131094
    private toHex(x: any) {
        return x.reduce((memo: any, i: any) => {
            return memo + ('0' + i.toString(16)).slice(-2);
        }, '');
    }
}