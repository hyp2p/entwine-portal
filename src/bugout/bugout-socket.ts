import { Duplex, DuplexOptions } from 'stream';

export default class BugoutSocket extends Duplex {
    public readonly rpcId: string;
    private bugoutClient: any;
    private interval: any;

    constructor(rpcId: string, bugoutClient: any, duplexOptions?: DuplexOptions) {
        super(duplexOptions);
        this.rpcId = rpcId;
        this.bugoutClient = bugoutClient;
    }

    _read(size: any) {
        try {
            this.bugoutClient.rpc(`readSock[${this.rpcId}]`, { size: size }, (res: any) => {
                if (res.error)
                    this.destroy(res.error)
                this.push(res.data);
                if (res.EOF) {
                    this.push(null);
                    setTimeout(() => {
                        this.emit('readytogo')
                    }, 0);
                }
            });
        } catch (e) {
            this.push(null);
            this.destroy(e);
        }
    }

    _write(chunk: any, encoding: any, callback: any) {
        if (Buffer.isBuffer(chunk))
            chunk = chunk.toString();
        try {
            this.bugoutClient.rpc(`writeSock[${this.rpcId}]`, { data: chunk, encoding }, (res: any) => {
                if (res.error)
                    this.destroy(res.error)
                callback();
            });
        } catch (e) {
            this.destroy(e);
        }

    }

    setTimeout(a?: any) {
        clearInterval(this.interval);
        console.log('We pretend to die', a);
    }

    setKeepAlive(a?: any) {
        this.interval = setInterval(function () {
            console.log('Some interval');
        }, 1000);
        console.log('We pretend to be alive', a);
    }

    unref(a?: any) {
        console.log('We pretend to unref', a);
    }

    ref(a?: any) {
        console.log('We pretend to ref', a);
    }
}