import { createServer } from 'http';
import * as dotenv from "dotenv";
dotenv.config();

import HttpServer from './localtunnel/http-server'

const server = createServer(HttpServer.app);

server.on('upgrade', HttpServer.handleWsUpgrade);

const port = process.env.PORT;
if (port) {
    server.listen(port, () => {
        console.log('Server started port:' + port);
    });
} else {
    console.log('Sever not started: Missing port variable');
}
