const _btt = require('bittorrent-tracker').Server;
import { Server } from 'ws';

const btt = new _btt({ http: false, udp: false, ws: false });

const onWebSocketConnection = btt.onWebSocketConnection.bind(btt);
const _onError = btt._onError.bind(btt);
const wss = new Server({ noServer: true });

wss.on('error', err => {
    console.log("Error handling connection");
    _onError(err)
})
wss.on('connection', (socket: any, req: any) => {
    console.log("Handling connection",);
    socket.upgradeReq = req
    onWebSocketConnection(socket);
})

export default wss;