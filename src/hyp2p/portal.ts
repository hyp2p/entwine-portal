import Bugout from '../bugout/bugout';
import BugoutSocket from '../bugout/bugout-socket';
const WebTorrent = require("webtorrent-hybrid");
import { createServer, Server } from "net";
import debug from "debug";

export namespace Portal {
    const log: debug.Debugger = debug("bugout");
    let wt: any = null;

    export function init(subdomain: string, address: string): Server {
        if (!wt) {
            wt = new WebTorrent();
        }
        const trackers = getTrackers();
        console.log(`Using trackers: ${trackers}`);
        return registerServer(subdomain,
            new Bugout(address,
                { wt: wt, announce: trackers, seed: null }));
    }

    export function getTrackers(): string[] | null {
        if (process.env.TRACKERS && process.env.TRACKERS.length > 0) {
            if (process.env.TRACKERS.includes(',')) {
                return process.env.TRACKERS.split(',');
            } else {
                return [process.env.TRACKERS]
            }
        } return [];
    }

    function registerServer(subdomain: string, bugoutClient: Bugout): Server {
        const server: Hyp2pServer = <Hyp2pServer>createServer();
        server.bugoutClient = bugoutClient;
        server.waitingCreateConnCount = 0;
        server.availableSockets = [];
        server.connectedSockets = 0;
        console.log(`Address:[${bugoutClient.getAddress()}] Seed:[${bugoutClient.seed}]`);
        server.bugoutClient.on("server", (addr: string) => {
            server.isBugoutClientOnline = true;
            server.emit("online");
        });
        server.bugoutClient.on("seen", console.log.bind(console, "Seen:"));
        server.on("awaitingSocket", async () => {
            console.log(`Socket is awaiting`);
            let sock = server.availableSockets.shift();
            let stillOn = sock?.writable && sock.readable;
            while (!stillOn && sock) {
                sock = server.availableSockets.shift();
                stillOn = sock && sock.writable && sock.readable;
            }
            if (!sock) {
                ++server.waitingCreateConnCount;
                if (server.isBugoutClientOnline) {
                    server.bugoutClient?.rpc("entangleSocks", { socks: 5 }, (res: { sockIds: string[], error: any }) => {
                        if (res.error)
                            throw 'Error: entangleSocks got:' + res.error;
                        console.log(res);
                        for (let i = 0; i < res.sockIds.length; i++) {
                            createConnection(server, subdomain, res.sockIds[i]);
                        }
                    });
                }
            } else {
                server.emit("freeSocket", sock);
            }
        });
        server.on("socketReady", () => {
            console.log("Socket became ready, pending queue: " + server.waitingCreateConnCount);
            if (server.waitingCreateConnCount > 0) {
                const sock = server.availableSockets.shift();
                if (sock && sock.writable && sock.readable) {
                    server.emit("freeSocket", sock);
                    --server.waitingCreateConnCount;
                } else {
                    throw 'Error: Invalid socket state, check connectivity issues';
                }
            }
        });
        server.on("online", () => {
            console.log("client online %s", subdomain);
            server.bugoutClient?.rpc("entangleSocks", { socks: server.waitingCreateConnCount + 2 },
                (res: { sockIds: string[], error: any }) => {
                    if (res.error)
                        throw 'Error: entangleSocks got:' + res.error;
                    console.log(res);
                    for (let i = 0; i < res.sockIds.length; i++) {
                        createConnection(server, subdomain, res.sockIds[i]);
                    }
                });
        });
        server.on("offline", () => {
            console.log("client offline %s", subdomain);
        });
        server.on("close", () => {
            server.bugoutClient?.close((err) => {
                console.log("BugoutClient closed. " + err ? "error:" + err : "");
            });
        })
        return server;
    }


    function createConnection(server: Hyp2pServer, subdomain: string, rpcId: string) {
        console.log(`creating connection for rpcId ${rpcId}`)
        for (const sock of server.availableSockets) {
            if (sock.rpcId == rpcId) {
                console.log(`Agent: Socket already taken [${rpcId}]`);
                return false;
            }
        }
        const socket = new BugoutSocket(rpcId, server.bugoutClient);
        socket.once("close", () => {
            console.log("Agent:socket closed");
            server.connectedSockets -= 1;
        });
        socket.once("readytogo", () => {
            createConnection(server, subdomain, rpcId);
            console.log("Agent: connected sockets: %s", server.connectedSockets);
        });
        socket.once("error", (err: any) => {
            console.log("Error: socket got [%s]", err);
            if (typeof err == "string" && err.includes("No such API call.")) {
                server.availableSockets = [];
                server.connectedSockets = 0;
            } else {
                const idx = server.availableSockets.indexOf(socket);
                if (idx >= 0) {
                    server.availableSockets.splice(idx, 1);
                }
            }
            if (server.connectedSockets <= 0) {
                server.emit("offline");
            }
        });
        server.connectedSockets += 1;
        server.availableSockets.push(socket);
        setTimeout(() => {
            server.emit("socketReady");
        }, 0);

    }

    class Hyp2pServer extends Server {
        public bugoutClient: Bugout | null = null;
        public waitingCreateConnCount: number = 0;
        public isBugoutClientOnline: boolean = false;
        public connectedSockets: number = 0;
        public availableSockets: BugoutSocket[] = [];
    }
}