var b;
var testResponse = `HTTP/1.0 200 OK
Date: Fri, 31 Dec 2003 23:59:59 GMT
Content-Type: text/html
Connection: close

<!doctype html>
<html>
<head>
    <title>Example Domain</title>

    <meta charset="utf-8" />
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <style type="text/css">
    body {
        background-color: #f0f0f2;
    }
    </style>
</head>

<body>
<div>
    <h1>Catnip arte y jardin</h1>
    <p>This domain is established to be used for illustrative examples in documents.
</div>
</body>
</html>


`;
function init() {

    const { Polly } = PollyJS;
    const polly = new Polly('Simple Example', {
        adapters: ['fetch', 'xhr'],
        persister: 'local-storage',
        logging: true
    });
    const { server } = polly;

    server.get('/test').intercept((req, res) => {
        res.status(200);
        res.json({ data: "Catnip it's alive!" });
    });

    var data = JSON.parse(document.getElementById('payload').dataset.data);
    var sdInput = document.getElementById('subdomain');
    sdInput.addEventListener('keyup', storeSubdomain);
    if (localStorage["lastSubDomain"]) {
        sdInput.value = localStorage["lastSubDomain"];
    } else {
        var subdomain = window.location.host.split('.')[1] ? window.location.host.split('.')[0] : '';
        localStorage["lastSubDomain"] = subdomain;
        sdInput.value = subdomain;
    }
    M.updateTextFields();
    b = new Bugout({ seed: localStorage["bugout-seed"], announce: data.tr });
    localStorage["bugout-seed"] = b.seed;
    log("address:", b.address(), "trackers:", data.tr ? data.tr : 'default', "announcing...");
    b.register("entangleSocks", (clientAddr, args, cb) => {
        const sockIds = [];
        for (let i = 0; i < args.socks; i++) {
            var uuid = uuidv4();
            b.register("readSock[" + uuid + "]", (_clientAddr, _args, _cb) => {
                // fetch('/test').then(d => {
                //     console.log(`Got answer from /test about to callback:`, d);
                //     cb({ data: d.body, EOD: true, error: null });
                // });
                _cb({ data: testResponse, EOF: true, error: null });
            });
            b.register("writeSock[" + uuid + "]", (_clientAddr, _args, _cb) => {
                _cb({ resp: 'ok' });
            });
            sockIds.push(uuid);
        }
        cb({ sockIds, error: null });
    });
    b.register("releaseSocket", (clientAddr, args, cb) => {
        //b.unregister(args.sockId, (error:any)=>{ cb({ error }); });
        cb({ error: null });
    });
    b.register("readSock[1]", (clientAddr, args, cb) => {
        cb({ data: testResponse, EOF: true, error: null });
    });
    b.register("writeSock[1]", (clientAddr, args, cb) => {
        cb({ resp: 'ok' });
    });
    b.on("connections", (c) => {
        log("connections:", c);
    });
    b.on("message", (address, msg) => {
        log("message:", address, msg);
    });
    b.on("rpc", (address, call, args) => { log("rpc:", address, call); });
    b.on("seen", (address) => { log("seen:", address); });
}

function entanglePortal() {
    const subdomain = localStorage["lastSubDomain"];
    if (subdomain && subdomain.length > 0) {
        fetch(`/hyp2p/entangle?subdomain=${subdomain}&address=${b.address()}`, { method: "GET" })
            .then((response) => response.json())
            .then((response) => {
                M.toast({ html: `Updated successfully` });
                log(`Assigned ${b.address()} to [${subdomain}] subdomain, response: [${JSON.stringify(response)}]`);
                var elBtn = document.getElementById('update-btn');
                if (elBtn) {
                    elBtn.classList.remove("pulse");
                }
            }).catch((error) => {
                log(`Error trying to entangle portal ${JSON.stringify(error)}`);
            });
    } else {
        M.toast({ html: 'Subdomain is required' });
    }
}

function storeSubdomain() {
    localStorage["lastSubDomain"] = document.getElementById('subdomain').value;
    var elBtn = document.getElementById('update-btn');
    if (elBtn) {
        elBtn.classList.add("pulse");
    }
}

function log() {
    var logContainer = document.getElementById("log");
    if (logContainer) {
        var args = Array.prototype.slice.call(arguments);
        args = args.map(function (a) { if (typeof (a) == "string") return a; else return JSON.stringify(a); });
        args.unshift(`[${new Date().toISOString()}]`);
        logContainer.textContent += args.join(" ") + "\n";
        logContainer.scrollTop = logContainer.scrollHeight;
        // console.log.apply(null, args);
    }
}

function rawRequest(txt, cb) {
    var lines = txt.split("\n"),
        methods = [
            "GET",
            "POST",
            "PATCH",
            "PUT",
            "DELETE",
            "HEAD",
            "OPTIONS"
        ],
        host, path, method, version, body = "", headers = {};
    lines.forEach((x, i) => {
        if (!x.includes(":")) {
            // let ind;
            methods.forEach(m => {
                let tmpIndex = x.indexOf(m);

                if (tmpIndex > -1) {
                    if (!method) {
                        // ind = tmpIndex;
                        let words = x.split(" ");
                        method = x.substring(
                            tmpIndex,
                            tmpIndex +
                            m.length
                        );
                        method = method && method.trim();
                        path = words.find((y, k) =>
                            y[0] === "/"
                        )
                        path = path && path.trim();
                        version = (
                            x
                                .replace(method, "")
                                .replace(path, "")
                        ).trim();
                    }

                }
            });
        } else {
            let indexOfSemiColon = x.indexOf(":");
            if (
                indexOfSemiColon > 0 &&
                indexOfSemiColon < x.length - 1
            ) {

                let key = x.substring(
                    0,
                    indexOfSemiColon
                ).trim(),
                    value = x.substring(
                        indexOfSemiColon + 1
                    ).trim();
                headers[key] = value;
                if (key.toLowerCase() == "host") {
                    host = value
                }
            }
        }
    });
    let inds = []
    txt.split(/\r?\n/).forEach((x, i) =>
        x === ""
        && inds.push(i)
    )
    let afterTwoLineBreaksIndex;
    inds.forEach((x, i) => {
        if (
            i < inds.length - 2 &&
            inds[i] === "" &&
            inds[i + 1] === ""
        ) {
            afterTwoLineBreaksIndex = i + 1;
        }
    });
    if (afterTwoLineBreaksIndex) {
        body = txt.substring(
            afterTwoLineBreaksIndex
        )
    }
    if (host && path && method) {
        x.open(
            method,
            "http://" //don't know how to differentiate between http & https, if some1 could help out would be greate
            + host
            + path
        );

        for (let k in headers) {
            x.setRequestHeader(k, headers[k]);
        }

        x.send(body);
    }
    return {
        headers,
        body,
        method,
        host,
        path,
        version
    }
}
