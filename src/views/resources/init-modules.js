window.PollyJS = window['@pollyjs/core'];
PollyJS.Polly.register(window['@pollyjs/adapter-fetch']);
PollyJS.Polly.register(window['@pollyjs/adapter-xhr']);
PollyJS.Polly.register(window['@pollyjs/persister-local-storage']);