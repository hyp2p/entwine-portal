define('my-counter', {
    attachShadow: { mode: 'open' },
    init() {
        this.count = 0;
        this.dec = () => { this.count--; this.render(); };
        this.inc = () => { this.count++; this.render(); };
        this.render();
    },
    render() {
        this.html`
        <style>
        * {
          font-size: 200%;
        }
        span {
          width: 4rem;
          display: inline-block;
          text-align: center;
        }
        button {
          width: 64px;
          height: 64px;
          border: none;
          border-radius: 10px;
          background-color: seagreen;
          color: white;
        }
        </style>
        <button onclick="${this.dec}">-</button>
        <span>${this.count}</span>
        <button onclick="${this.inc}">+</button>
      `;
    }
});