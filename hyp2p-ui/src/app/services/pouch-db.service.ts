import { Injectable } from '@angular/core';
import PouchDB from 'pouchdb';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class PouchDBService {
    private instances: Map<string, PouchDB.Database>;
    private behaviorSubjects: Map<string, BehaviorSubject<string>>;
    private dbUpdates: Map<string, Observable<string>>;

    constructor() {
        this.instances = new Map();
        this.behaviorSubjects = new Map();
        this.dbUpdates = new Map();
     }

    public getInstance(name: string) {
        if(this.instances.has(name)){
            return this.instances.get(name);
        }
        this.instanciate(name);
        return this.instances.get(name);
    }
    public getDbUpdates(name: string) {
        if(this.dbUpdates.has(name)){
            return this.dbUpdates.get(name);
        }
        this.instanciate(name);
        return this.dbUpdates.get(name);
    }

    private instanciate(name: string){
        const p = new PouchDB(name);
        const dbUpdatesSubject = new BehaviorSubject<string>('0');
        const dbUpdates$ = dbUpdatesSubject.asObservable();
        p.info().then((info) => {
            p.changes({
                since: 'now',
                live: true
            }).on('change', () => {
                dbUpdatesSubject.next(`${new Date().getTime()}`);
            });
        })
        this.instances.set(name, p);
        this.behaviorSubjects.set(name, dbUpdatesSubject);
        this.dbUpdates.set(name, dbUpdates$);
    }
}
