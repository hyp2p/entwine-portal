import { Injectable } from '@angular/core';
import { PouchDBService } from './pouch-db.service';
import PouchDB from 'pouchdb';
@Injectable({
    providedIn: 'root'
})
export class SiteService {
    private db:PouchDB.Database<Site>;
    constructor(private pouchService: PouchDBService) {
        this.db = <PouchDB.Database<Site>>pouchService.getInstance('sites');
    }

    public create(name: string, components: any, html: string | null, css: string | null, js: string | null): Promise<string> {
        const site: Site = {
            _id: btoa(name),
            name: name,
            updated: new Date()
        };
        site.components = components ? JSON.stringify(components) : '';
        site._attachments = {
            "page.html": {
                content_type: "text/plain",
                data: new Blob(
                    [html || '<html>Sitio en construcción</html>'],
                    { type: 'text/plain' })
            },
            "page.css": {
                content_type: "text/plain",
                data: new Blob(
                    [css || 'html{background:white}'],
                    { type: 'text/plain' })
            },
            "page.js": {
                content_type: "text/plain",
                data: new Blob(
                    [js || 'function hello(){console.log("initialized")}'],
                    { type: 'text/plain' })
            }
        }
        return this.db.put<Site>(site).then((value: PouchDB.Core.Response): string => {
            return value.id;
        });
    }

    public update(name: string): Promise<boolean> {
        return this.db.get<Site>(btoa(name)).then((doc) => {
            doc.name = name;
            doc.updated = new Date();
            return this.db.put<Site>(doc).then((value: PouchDB.Core.Response) => {
                return value.ok;
            });
        });
    }

    public publishHTML(id: string, components: any, html: string, css: string | null, js: string | null): Promise<boolean> {
        return this.db.get<Site>(id).then((doc) => {
            doc.components = components ? JSON.stringify(components) : '';
            doc._attachments = {
                "page.html": {
                    content_type: "text/plain",
                    data: new Blob(
                        [html],
                        { type: 'text/plain' })
                },
                "page.css": {
                    content_type: "text/plain",
                    data: new Blob(
                        [css],
                        { type: 'text/plain' })
                },
                "page.js": {
                    content_type: "text/plain",
                    data: new Blob(
                        [js],
                        { type: 'text/plain' })
                }
            }
            return this.db.put(doc).then(res => {
                return res.ok;
            }).catch(e => {
                console.log(e);
                return false;
            });
        }).catch(e => {
            console.log(e);
            return false;
        });

    }

    public all(attachments?: boolean): Promise<Site[]> {
        return this.db.allDocs<Site>({ include_docs: true, attachments: attachments })
            .then((res: PouchDB.Core.AllDocsResponse<Site>) => {
                return res.rows.map((row) => {
                    return row.doc ? row.doc :
                        {
                            _id: row.id,
                            _rev: row.value.rev,
                            name: null
                        }
                });
            });
    }

    public get(name: string): Promise<Site> {
        return this.db.get<Site>(btoa(name)).then((site: Site) => {
            console.log(site);
            return site;
        });
    }

    public getById(id: string, attachments?: boolean): Promise<Site> {
        return this.db.get<Site>(id, { attachments: attachments }).then((site: Site) => {
            site.components = site.components ? JSON.parse(site.components) : {};
            return site;
        });
    }

    public delete(id: string, rev: string): Promise<boolean> { //{_id: todo._id, _rev: todo._rev}
        return this.db.remove({ _id: id, _rev: rev }).then((res) => {
            return res.ok;
        }).catch(e => {
            console.log(e);
            return false;
        });
    }
}

export interface Site {
    _id?: string,
    name: string,
    components?: string,
    _attachments?: any,
    _rev?: string,
    updated?: Date;
}