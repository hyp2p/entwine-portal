import { Injectable, isDevMode } from '@angular/core';
import BugoutInstance from '../../../../src/bugout/bugout';
const _Bugout = require('../../assets/bugout');
import { BehaviorSubject } from 'rxjs';
declare const WebTorrent: any;
import { environment } from '../../environments/environment';
@Injectable({
    providedIn: 'root'
})
export class Hyp2pService {
    private wt: any;
    private bugoutServers: Map<string, BugoutInstance>;
    private dbUpdatesSubject = new BehaviorSubject<string>('offline *');
    public dbUpdates$ = this.dbUpdatesSubject.asObservable();
    private baseWsUrl: string;

    constructor() {
        this.bugoutServers = new Map();
        this.wt = new WebTorrent();
        const parsedUrl = new URL(window.location.href);
        const baseUrl = parsedUrl.origin;
        this.baseWsUrl = baseUrl.replace('https', 'wss').replace('http', 'ws') + '/api';
    }

    public disableServer(name: string) {
        if (this.bugoutServers.has(name)) {
            const srv = this.bugoutServers.get(name);
            srv.destroy(() => {
                this.dbUpdatesSubject.next('offline ' + name);
            });
            this.bugoutServers.delete(name);
        }
    }

    public enableServer(name: string, seed?: string) {
        if (this.bugoutServers.has(name)) {
            return this.bugoutServers.get(name);
        }
        const bgSrv: BugoutInstance = this.initBugoutHandlers(name, seed);
        this.bugoutServers.set(name, bgSrv);
        return bgSrv;
    }

    private initBugoutHandlers(name: string, seed?: string): any {
        const trackers = [this.baseWsUrl, ...environment.trackers];
        const b: BugoutInstance = new _Bugout.default({ wt: this.wt, seed: seed, announce: trackers });
        this.log("address", b.getAddress(), "trackers", trackers);
        this.dbUpdatesSubject.next(`${name} online: ${b.getAddress()}`);
        b.register("readSock[1]", (clientAddr, args, cb) => {
            cb({ data: 'ok', EOF: true, error: null });
        });
        b.register("writeSock[1]", (clientAddr, args, cb) => {
            cb({ resp: 'ok' });
        });
        b.on("connections", (c) => {
            this.log("connections:", c);
        });
        b.on("message", (address, msg) => {
            this.log("message:", address, msg);
        });
        b.on("rpc", (address, call, args) => { this.log("rpc:", address, call); });
        b.on("seen", (address) => { this.log("seen:", address); });
        return b;
    }

    private log(...args: any[]) {
        args = args.map((a) => { if (typeof (a) == "string") return a; else return JSON.stringify(a); });
        args.unshift(`[${new Date().toISOString()}]`);
        console.log(args.join(" "));
    }

}
