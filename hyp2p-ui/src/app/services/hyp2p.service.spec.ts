import { TestBed } from '@angular/core/testing';

import { Hyp2pService } from './hyp2p.service';

describe('Hyp2pService', () => {
  let service: Hyp2pService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(Hyp2pService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
