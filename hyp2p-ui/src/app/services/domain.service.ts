import { Injectable } from '@angular/core';
import Bugout from '../../../../src/bugout/bugout';
import { Hyp2pService } from './hyp2p.service';
import { PouchDBService } from './pouch-db.service';
import { v4 as uuidv4 } from 'uuid';
import PouchDB from 'pouchdb';

@Injectable({
    providedIn: 'root'
})
export class DomainService {
    private db: PouchDB.Database<Domain>;
    private testResponse = `HTTP/1.0 200 OK
Date: Fri, 31 Dec 2003 23:59:59 GMT
Content-Type: text/html
Connection: close

<!doctype html>
<html>
<head>
    <title>Example Domain</title>

    <meta charset="utf-8" />
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <style type="text/css">
    body {
        background-color: #f0f0f2;
    }
    </style>
</head>

<body>
<div>
    <h1>Catnip arte y jardin</h1>
    <p>This domain is established to be used for illustrative examples in documents.
</div>
</body>
</html>


`;
    private pendingCallbacks: Map<string, Function>;
    private pendingResponsesTobeSent: Map<string, any>;

    constructor(private pouchService: PouchDBService, private hyp2pService: Hyp2pService) {
        this.pendingCallbacks = new Map();
        this.pendingResponsesTobeSent = new Map();
        this.db = <PouchDB.Database<Domain>>this.pouchService.getInstance('domains');
        this.all(false).then((sites: Domain[]) => {
            sites.forEach((domain: Domain) => {
                if (domain.enabled) {
                    this.launchSequence(domain.name, domain.seed);
                }
            });
        });
    }

    private create(name: string, description: string, seed: string, secure: boolean): Promise<string> {
        const doc: Domain = {
            _id: btoa(name),
            name: name,
            updated: new Date(),
            description: description,
            seed: seed,
            secure: secure
        };
        return this.db.put<Domain>(doc).then((value: PouchDB.Core.Response): string => {
            return value.id;
        });
    }

    public launchSequence(name: string, seed: string) {
        const srv = this.hyp2pService.enableServer(name, seed);
        if (srv) {
            this.augmentHandlers(srv);
        }
    }

    public toggleEnable(name: string, enabled: boolean): Promise<{ ok: boolean, address: string }> {
        if (enabled) {
            return this.db.get<Domain>(btoa(name)).then((doc) => {
                doc.name = name;
                doc.enabled = enabled;
                doc.updated = new Date();
                let srv;
                if (doc.seed && doc.seed.length > 0) {
                    srv = this.hyp2pService.enableServer(name, doc.seed);
                } else {
                    srv = this.hyp2pService.enableServer(name, undefined);
                    doc.address = srv.getAddress();
                    doc.seed = srv.seed;
                }
                if (srv) {
                    this.augmentHandlers(srv);
                }
                return this.db.put<Domain>(doc).then((value: PouchDB.Core.Response) => {
                    return { ok: value.ok, address: doc.address };
                });
            });
        } else {
            return this.db.get<Domain>(btoa(name)).then((doc) => {
                this.hyp2pService.disableServer(name);
                doc.enabled = enabled;
                doc.updated = new Date();
                return this.db.put<Domain>(doc).then((value: PouchDB.Core.Response) => {
                    return { ok: value.ok, address: null };
                });
            });
        }

    }

    private augmentHandlers(b: Bugout) {
        b.register("entangleSocks", (clientAddr: string, args: any, cb: Function) => {
            const sockIds = [];
            for (let i = 0; i < args.socks; i++) {
                const uuid = uuidv4();
                b.register("readSock[" + uuid + "]", (_clientAddr: string, _args: any, _cb: Function) => {
                    this.handleResponseCallback(uuid, _cb);
                });
                b.register("writeSock[" + uuid + "]", (_clientAddr: string, _args: any, _cb: Function) => {
                    _cb({ resp: this.handleRequestString(uuid, _args) });
                });
                sockIds.push(uuid);
            }
            cb({ sockIds, error: null });
        });
        b.register("releaseSocket", (clientAddr, args, cb) => {
            //b.unregister(args.sockId, (error:any)=>{ cb({ error }); });
            cb({ error: null });
        });
    }

    private handleResponseCallback(uuid: string, cb: Function) {
        if (!this.pendingResponsesTobeSent.has(uuid)) {
            this.pendingCallbacks.set(uuid, cb);
            return;
        }
        const res = this.pendingResponsesTobeSent.get(uuid);
        try {
            cb(res);
        } catch (e) { console.log(e); }
        finally {
            this.pendingResponsesTobeSent.delete(uuid);
        }
    }

    private handleRequestString(uuid: string, req: any) {
        //do something async and then call fill request;
        this.fillResponse(uuid, this.testResponse);
        return 'ok';
    }

    public fillResponse(uuid: string, res: any) {
        if (this.pendingCallbacks.has(uuid)) {
            const cb = this.pendingCallbacks.get(uuid);
            try {
                cb({ data: res, EOF: true, error: null });
            } catch (e) {
                console.log(e);
            }
            finally {
                this.pendingCallbacks.delete(uuid);
            }
        }
    }

    public upsert(name: string, description: string, seed: string, secure: boolean): Promise<string> {
        return this.db.get<Domain>(btoa(name)).then((doc) => {
            if (doc && doc._id) {
                doc.name = name;
                doc.description = description;
                doc.seed = seed;
                doc.secure = secure;
                doc.updated = new Date();
                return this.db.put<Domain>(doc).then((value: PouchDB.Core.Response) => {
                    return doc._id;
                });
            } else {
                return this.create(name, description, seed, secure).then((value: string) => {
                    return value;
                });
            }
        }).catch((err) => {
            if (err.name == "not_found") {
                return this.create(name, description, seed, secure).then((value: string) => {
                    return value;
                });
            }
            return err;
        });
    }

    public all(attachments?: boolean): Promise<Domain[]> {
        return this.db.allDocs<Domain>({ include_docs: true, attachments: attachments })
            .then((res: PouchDB.Core.AllDocsResponse<Domain>) => {
                return res.rows.map((row) => {
                    return row.doc ? row.doc :
                        {
                            _id: row.id,
                            _rev: row.value.rev,
                            name: null
                        }
                });
            });
    }

    public getByName(name: string, attachments?: boolean): Promise<Domain> {
        return this.db.get<Domain>(btoa(name), { attachments: attachments }).then((domain: Domain) => {
            return domain;
        });
    }

    public delete(name: string, rev: string): Promise<boolean> {
        return this.db.remove({ _id: btoa(name), _rev: rev }).then((res) => {
            return res.ok;
        }).catch(e => {
            console.log(e);
            return false;
        });
    }

    public syncDomain(name: string, address: string): Promise<boolean> {
        return fetch(`/api/entangle?hyp2p=entangle&subdomain=${name}&address=${address}`, { method: "GET" })
            .then((response) => response.json())
            .then((response) => {
                return true;
            }).catch((error) => {
                console.log(error);
                return false;
            });
    }
}

export interface Domain {
    _id?: string,
    name: string,
    description?: string,
    secure?: boolean,
    seed?: string,
    address?: string,
    enabled?: boolean,
    _attachments?: any,
    _rev?: string,
    updated?: Date,
    wires?: number,
    viewCount?: number,
    backups?: number
}