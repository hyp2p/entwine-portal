import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatSidenavModule } from '@angular/material/sidenav';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { MatTooltipModule } from '@angular/material/tooltip';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { RouterModule } from '@angular/router';
import { ServerComponent } from './components/server/server.component';
import { SiteCreatorComponent } from './components/site-creator/site-creator.component';
import { StatisticsComponent } from './components/statistics/statistics.component';
import { PaymentsComponent } from './components/payments/payments.component';
import { SecurityComponent } from './components/security/security.component';
import { SettingsComponent } from './components/settings/settings.component';
import { MatTableModule } from '@angular/material/table';
import { HttpClientModule } from '@angular/common/http';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatToolbarModule } from '@angular/material/toolbar';
import { PortalModule } from '@angular/cdk/portal';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatChipsModule } from '@angular/material/chips';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTabsModule } from '@angular/material/tabs';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatCardModule } from '@angular/material/card';
import { PouchDBService } from './services/pouch-db.service';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { Hyp2pService } from './services/hyp2p.service';
import { DomainService } from './services/domain.service';

@NgModule({
    declarations: [
        AppComponent,
        DashboardComponent,
        ServerComponent,
        SiteCreatorComponent,
        StatisticsComponent,
        PaymentsComponent,
        SecurityComponent,
        SettingsComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule, RouterModule, MatTableModule, HttpClientModule,
        ServiceWorkerModule.register('ngsw-worker.js', {
            enabled: environment.production,
            registrationStrategy: 'registerImmediately'
        }),
        BrowserAnimationsModule, MatSidenavModule, MatButtonModule, MatMenuModule,
        MatIconModule, MatDividerModule, MatTooltipModule, MatProgressSpinnerModule,
        MatPaginatorModule, MatToolbarModule, PortalModule, MatDialogModule, MatInputModule,
        MatChipsModule, FormsModule, ReactiveFormsModule, MatSnackBarModule, MatTabsModule,
        MatCheckboxModule, MatCardModule, MatSlideToggleModule
    ],
    providers: [PouchDBService, Hyp2pService, DomainService],
    bootstrap: [AppComponent]
})
export class AppModule { }
