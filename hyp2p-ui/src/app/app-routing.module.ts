import { SettingsComponent } from './components/settings/settings.component';
import { SecurityComponent } from './components/security/security.component';
import { PaymentsComponent } from './components/payments/payments.component';
import { StatisticsComponent } from './components/statistics/statistics.component';
import { SiteCreatorComponent } from './components/site-creator/site-creator.component';
import { ServerComponent } from './components/server/server.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
    {
        path: 'dashboard',
        component: DashboardComponent
    }, {
        path: 'server',
        component: ServerComponent
    }, {
        path: 'site-creator',
        component: SiteCreatorComponent
    }, {
        path: 'statistics',
        component: StatisticsComponent
    }, {
        path: 'payments',
        component: PaymentsComponent
    }, {
        path: 'security',
        component: SecurityComponent
    }, {
        path: 'settings',
        component: SettingsComponent
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
