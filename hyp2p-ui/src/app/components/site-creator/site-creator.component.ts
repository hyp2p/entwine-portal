import { Site, SiteService } from './../../services/site.service';
import { Portal, TemplatePortal } from '@angular/cdk/portal';
import { HttpClient } from '@angular/common/http';
import { Component, ViewChild, AfterViewInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import grapesjs from 'grapesjs';
import 'grapesjs-preset-webpage';
import navbar from 'grapesjs-navbar';
import countdown from 'grapesjs-component-countdown';
import indexeddb from 'grapesjs-indexeddb';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { FormControl, Validators } from '@angular/forms';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';

@Component({
    selector: 'app-site-creator',
    templateUrl: './site-creator.component.html',
    styleUrls: ['./site-creator.component.scss']
})
export class SiteCreatorComponent implements AfterViewInit {
    @ViewChild('listPortalContent') listPortalContent: TemplateRef<unknown>;
    @ViewChild('upsertPortalContent') upsertPortalContent: TemplateRef<unknown>;
    @ViewChild('previewPortalContent') previewPortalContent: TemplateRef<unknown>;
    @ViewChild('listPortalButtonsContent') listPortalButtonsContent: TemplateRef<unknown>;
    @ViewChild('upsertPortalButtonsContent') upsertPortalButtonsContent: TemplateRef<unknown>;
    @ViewChild('previewPortalButtonsContent') previewPortalButtonsContent: TemplateRef<unknown>;
    @ViewChild('newSiteDialog') newSiteDialog: TemplateRef<unknown>;
    selectedPortal: Portal<any> = null;
    selectedButtonsPortal: Portal<any> = null;
    listPortal: TemplatePortal<any>;
    upsertPortal: TemplatePortal<any>;
    previewPortal: TemplatePortal<any>;
    listButtonsPortal: TemplatePortal<any>;
    upsertButtonsPortal: TemplatePortal<any>;
    previewButtonsPortal: TemplatePortal<any>;
    displayedColumns: string[] = ['name', 'updated', 'actions'];
    data: MatTableDataSource<Site> = new MatTableDataSource([]);
    resultsLength = 0;
    isLoadingResults = true;
    @ViewChild(MatSort) sort: MatSort;
    nameFormControl = new FormControl('', [
        Validators.required
    ]);
    locFormControl = new FormControl('', [
        Validators.required
    ]);
    searching = false;
    visible = true;
    selectable = true;
    removable = true;
    addOnBlur = true;
    readonly separatorKeysCodes: number[] = [ENTER, COMMA];
    private images = [];
    private pageId = 'some-id';
    private _editor: any;
    get editor() {
        return this._editor;
    }

    constructor(private _httpClient: HttpClient, private _router: Router, private _snackBar: MatSnackBar,
        private _viewContainerRef: ViewContainerRef, private _siteDB: SiteService,
        private _route: ActivatedRoute, public _dialog: MatDialog) {
    }

    ngAfterViewInit(): void {
        this.initializeData();
        this.initializePortals();
        this.initializeQueryObserver();
    }

    private initializeQueryObserver() {
        this._route.queryParams.subscribe(params => {
            if (params['pageId']) {
                this.pageId = params['pageId'];
                setTimeout(() => {
                    this.selectPortalToDisplay(this.upsertPortal, this.upsertButtonsPortal);
                });
            } else {
                this.pageId = '';
                setTimeout(() => {
                    this.selectPortalToDisplay(this.listPortal, this.listButtonsPortal)
                });
            }
        });
    }

    public displayList() {
        this._router.navigate([], {
            relativeTo: this._route, queryParams: {}
        });
    }

    private initializePortals() {
        this.listPortal = new TemplatePortal(
            this.listPortalContent,
            this._viewContainerRef
        );
        this.upsertPortal = new TemplatePortal(
            this.upsertPortalContent,
            this._viewContainerRef
        );
        this.previewPortal = new TemplatePortal(
            this.previewPortalContent,
            this._viewContainerRef
        );
        this.listButtonsPortal = new TemplatePortal(
            this.listPortalButtonsContent,
            this._viewContainerRef
        );
        this.upsertButtonsPortal = new TemplatePortal(
            this.upsertPortalButtonsContent,
            this._viewContainerRef
        );
        this.previewButtonsPortal = new TemplatePortal(
            this.previewPortalButtonsContent,
            this._viewContainerRef
        );
        setTimeout(() => {
            this.selectedPortal = this.listPortal;
            this.selectedButtonsPortal = this.listButtonsPortal;
        });
    }

    private initializeData() {
        const _D = [];
        this._siteDB.all(true).then((sites: Site[]) => {
            sites.forEach((site: Site) => {
                _D.push({
                    _id: site._id,
                    _rev_: site._rev,
                    name: site.name,
                    updated: site.updated,
                    components: site.components ? JSON.parse(site.components) : {}
                })
            });
            this.data.data = _D;
            this.isLoadingResults = false;
        });


    }

    public selectPortalToDisplay(portal: TemplatePortal<any>, portalButtons: TemplatePortal<any>) {
        this.selectedPortal = portal;
        this.selectedButtonsPortal = portalButtons;
        if (this.upsertPortal == portal) {
            setTimeout(() => {
                this._editor = this.initializeGrapeJs();
                this._editor.on('asset:add', () => {
                    console.log('Asset add fired');
                    this._editor.runCommand('open-assets');
                });
                this._editor.Keymaps.removeAll();
                setTimeout(() => {
                    this.dragHandler(document.getElementsByClassName("gjs-pn-views-container")[0]);
                }, 1000);
            });
        } else if (this.listPortal == portal) {
            this.initializeData();
        }
    }

    public initializeGrapeJs() {
        return grapesjs.init({
            container: '#gjs',
            showToolbar: 0,
            allowScripts: 1,
            autorender: true,
            forceClass: false,
            components: '',
            style: '',
            plugins: ['gjs-preset-webpage',
                'grapesjs-tabs',
                'grapesjs-custom-code',
                'grapesjs-touch',
                'grapesjs-parser-postcss',
                'grapesjs-tooltip',
                navbar, countdown,
                'grapesjs-tui-image-editor',
                'grapesjs-typed',
                'grapesjs-style-bg',
                'grapesjs-plugin-forms', indexeddb],
            pluginsOpts: {
                'grapesjs-indexeddb': {
                    dbName: 'hyp2p',
                    objectStoreName: 'sites'
                },
                'gjs-preset-webpage': {
                    navbarOpts: false,
                    countdownOpts: false,
                    formsOpts: false,
                    blocksBasicOpts: {
                        blocks: ['link-block', 'quote', 'image', 'video', 'text', 'column1', 'column2', 'column3'],
                        flexGrid: true,
                        stylePrefix: 'hyp2p-'
                    }
                },
                'grapesjs-tabs': {
                    tabsBlock: {
                        category: 'Extra'
                    }
                },
                'grapesjs-typed': {
                    block: {
                        category: 'Extra',
                        content: {
                            type: 'typed',
                            'type-speed': 40,
                            strings: [
                                'Text row one',
                                'Text row two',
                                'Text row three',
                            ],
                        }
                    }
                }
            },
            assetManager: {
                embedAsBase64: 1,
                assets: this.images
            },
            canvas: {
                styles: [
                    'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css',
                    'https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css'
                ],
                scripts: ['https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js']
            },
            storageManager: {
                type: 'indexeddb',
                id: this.pageId,
            }
        });
    }

    private dragHandler(container) {
        const btns: any = document.getElementsByClassName("gjs-pn-views");
        const dragItem = document.createElement('span');
        if (btns && btns[0]) {
            dragItem.className += 'material-icons';
            dragItem.className += ' grab-handler';
            dragItem.innerHTML = 'drag_indicator'
            btns[0].prepend(dragItem);
            container.prepend(btns[0]);
        }

        let active = false;
        let currentX;
        let currentY;
        let initialX;
        let initialY;
        let xOffset = 0;
        let yOffset = 0;

        const dragStart = (e: any) => {
            if (e.type === "touchstart") {
                initialX = e.touches[0].clientX - xOffset;
                initialY = e.touches[0].clientY - yOffset;
            } else {
                initialX = e.clientX - xOffset;
                initialY = e.clientY - yOffset;
            }

            if (e.target === dragItem) {
                active = true;
            }
        }

        const dragEnd = (e) => {
            initialX = currentX;
            initialY = currentY;

            active = false;
        }

        const drag = (e) => {
            if (active) {
                e.preventDefault();
                if (e.type === "touchmove") {
                    currentX = e.touches[0].clientX - initialX;
                    currentY = e.touches[0].clientY - initialY;
                } else {
                    currentX = e.clientX - initialX;
                    currentY = e.clientY - initialY;
                }
                xOffset = currentX;
                yOffset = currentY;
                setTranslate(currentX, currentY, container);
            }
        }

        const setTranslate = (xPos, yPos, el) => {
            el.style.transform = "translate3d(" + xPos + "px, " + yPos + "px, 0)";
        }

        container.addEventListener("touchstart", dragStart, false);
        container.addEventListener("touchend", dragEnd, false);
        container.addEventListener("touchmove", drag, false);

        container.addEventListener("mousedown", dragStart, false);
        container.addEventListener("mouseup", dragEnd, false);
        container.addEventListener("mousemove", drag, false);
    }

    public createSite() {
        const dialogRef = this._dialog.open(this.newSiteDialog);
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.createSiteData(result.name, null, null, null, null);
            }
        });
    }

    public removeSite() {
        return this._siteDB.getById(this.pageId).then((site: Site) => {
            return this._siteDB.delete(site._id, site._rev).then((res) => {
                this._router.navigate([], {
                    relativeTo: this._route, queryParams: {}
                });
                return res;
            });
        });
    }

    public updateSite() {
        this._siteDB.getById(this.pageId).then((site: Site) => {
            this.nameFormControl.setValue(site.name);
            const data: any = site;
            data.isEdit = true;
            const dialogRef = this._dialog.open(this.newSiteDialog, { data: data });
            dialogRef.afterClosed().subscribe(async (result) => {
                if (result) {
                    if (btoa(result.loc) == site._id) {
                        this.updateSiteData(result.name);
                    } else {
                        const del = await this._siteDB.delete(site._id, site._rev).then((res) => {
                            return res;
                        });
                        const create = await this.createSiteData(result.name, this.editor.getWrapper(), 
                            this.editor.getHtml(), this.editor.getCss(), this.editor.getJs());
                        console.log(del, create)
                    }
                }
            });
        });
    }

    public openSite(id: string) {
        const queryParams: Params = { pageId: id };
        this._router.navigate([], {
            relativeTo: this._route, queryParams: queryParams, queryParamsHandling: 'merge',
        });
    }

    private createSiteData(name: string, components: any, html: string | null,
        css: string | null, js: string | null): Promise<string> {
        return this._siteDB.create(name, components, html, css, js).then((id: string) => {
            // const queryParams: Params = { pageId: id };
            // this._router.navigate([], {
            //     relativeTo: this._route, queryParams: queryParams, queryParamsHandling: 'merge',
            // });
            this.initializeData();
            return id;
        }).catch((e) => {
            console.log(e);
            return null;
        });
    }

    private updateSiteData(name: string) {
        this._siteDB.update(name).then((ok: boolean) => {
            this._snackBar.open(ok ? 'Actualizado correctamente.' : 'Error de actualización, reintentar.', 'Entendido', {
                duration: 15000
            });
        });
    }

    public upsertSiteHTML() {
        this._siteDB.publishHTML(this.pageId, this.editor.getComponents(),
            this.editor.getHtml(), this.editor.getCss(), this.editor.getJs()).then((res: boolean) => {
                this._snackBar.open(res ? 'Publicado correctamente.' : 'Error de publicación, reintentar.', 'Entendido', {
                    duration: 15000
                });
            });
    }

    public restore() {
        this._siteDB.getById(this.pageId, false).then((site) => {
            this._editor.setComponents(site.components);
        });
    }

    public applyFilter(event: any) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.data.filter = filterValue.trim().toLowerCase();
    }

}
export interface SiteApi {
    pages: Site[];
    total_count: number;
}
