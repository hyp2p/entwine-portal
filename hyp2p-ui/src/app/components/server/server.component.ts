import { DomainService } from './../../services/domain.service';
import { Component, Inject, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Domain } from 'src/app/services/domain.service';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { DOCUMENT } from '@angular/common';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
    selector: 'app-server',
    templateUrl: './server.component.html',
    styleUrls: ['./server.component.scss']
})
export class ServerComponent implements OnInit {
    public baseDomain: string;
    public basePort: string;
    @ViewChild('newDomainDialog') newDomainDialog: TemplateRef<unknown>;
    nameFormControl = new FormControl('', [
        Validators.required
    ]);
    domainList: Domain[] = [];

    tabs = [{
        name: 'Dominios'
    }];

    selected = new FormControl(0);

    constructor(public _dialog: MatDialog, private _dService: DomainService, @Inject(DOCUMENT) private document: any,
        private _snackBar: MatSnackBar) {
        this.baseDomain = this.document.location.hostname;
        this.basePort = this.document.location.port;
    }

    ngOnInit(): void {
        this.initializeData();
    }

    createDomain() {
        const dialogRef = this._dialog.open(this.newDomainDialog);
        dialogRef.afterClosed().subscribe(res => {
            if (res) {
                this._dService.upsert(res.name, '', undefined, false).then((r) => {
                    if (r && r.length > 0) {
                        this.domainList.push({ name: res.name, wires: 0, viewCount: 0, backups: 0, enabled: false });
                    }
                }).catch((err) => {
                    console.log(err);
                });;
            }
        });
    }

    openDomain(name: string) {
        let exists = false;
        let pos = 0;
        for (let t of this.tabs) {
            if (t.name === name) {
                exists = true;
                break;
            }
            ++pos;
        }
        if (exists) {
            this.selected.setValue(pos);
        } else {
            this.tabs.push({ name: name });
            this.selected.setValue(this.tabs.length - 1);
        }
    }

    private initializeData() {
        const _D = [];
        this._dService.all(false).then((sites: Domain[]) => {
            sites.forEach((domain: Domain) => {
                domain.wires = 0;
                domain.viewCount = 0;
                domain.backups = 0;
                _D.push(domain);
            });
            this.domainList = _D;
        });
    }

    deleteDomain(name: string) {
        return this._dService.getByName(name).then((d: Domain) => {
            return this._dService.delete(name, d._rev).then((res) => {
                this.initializeData();
                return res;
            });
        });

    }

    removeTab(index: number) {
        this.tabs.splice(index, 1);
    }

    visitSite(site: string) {
        window.open(`//${site}${this.baseDomain ? '.' + this.baseDomain : ''}${this.basePort ? ':' + this.basePort : ''}`, "_blank");
    }

    toggleEnable(name: string, enable: MatSlideToggleChange) {
        return this._dService.toggleEnable(name, enable.checked).then((res) => {
            if (res && res.ok) {
                const i = this.domainList.findIndex(d => d.name == name);
                if (i != -1) {
                    this.domainList[i].enabled = enable.checked;
                    this.domainList[i].address = res.address;
                }
                this._dService.syncDomain(name, res.address).then((res) => {
                    this._snackBar.open(res ? 'Actualización correcta.' : 'Error actualizando central, reintentar.', 'Entendido', {
                        duration: 15000
                    });
                });
                return res.ok;
            }
        });
    }

    syncDomain(name: string, address: string) {
        this._dService.syncDomain(name, address).then((res) => {
            this._snackBar.open(res ? 'Actualización correcta.' : 'Error actualizando central, reintentar.', 'Entendido', {
                duration: 15000
            });
        });
    }

}
