import { Component } from '@angular/core';
import { DomainService } from './services/domain.service';
declare var PollyJS: any;
const { Polly } = PollyJS;

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title = 'hyp2p-ui';
    showFiller = false;

    constructor(private _d: DomainService) {
        const polly = new Polly('Simple Example', {
            adapters: ['fetch', 'xhr'],
            persister: 'local-storage',
            logging: true
        });
        const { server } = polly;

        server.get('/test').intercept((req, res) => {
            res.status(200).send(`HTTP/1.0 200 OK
Date: Fri, 31 Dec 2003 23:59:59 GMT
Content-Type: text/html
Connection: close


`);
        });


        // this._hyp2p.entanglePortal

    }

}
